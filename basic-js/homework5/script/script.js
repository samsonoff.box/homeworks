let userNumber1;
let userNumber2;
let mathSymbol;

do {
    userNumber1 = prompt('Please enter your first number');
} while (isNaN(userNumber1) || userNumber1 === null || userNumber1.trim().length === 0);

do {
    userNumber2 = prompt('Please enter your second number');
} while (isNaN(userNumber2) || userNumber2 === null || userNumber2.trim().length === 0);

do {
    mathSymbol = prompt(`Please enter a mathematical symbol: '+', '-', '*' or '/'`);
    if (mathSymbol !== '+' && mathSymbol !== '-' && mathSymbol !== '*' && mathSymbol !== '/') {
        alert('The mathematical symbol that you have entered does not exist!');
    }
} while (mathSymbol !== '+' && mathSymbol !== '-' && mathSymbol !== '*' && mathSymbol !== '/');

function divisionOfTwoNumbers() {
    let result = userNumber1/userNumber2;
    console.log(result);
}

divisionOfTwoNumbers();

function MathOperation() {
    switch (mathSymbol) {
        case '+':
            console.log((+userNumber1) + (+userNumber2));
            break;
        case '-':
            console.log((+userNumber1) - (+userNumber2));
            break;
        case '*':
            console.log((+userNumber1) * (+userNumber2));
            break;
        case '/':
            console.log((+userNumber1) / (+userNumber2));
            break;
    }
}

MathOperation();