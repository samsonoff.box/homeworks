Теоретичні питання
1. Як можна сторити функцію та як ми можемо її викликати?
In order to create a function, we use the 'function' keyword. Then we assign a name to the function, and then we use the braces for the function body. in order to call the function, we use the empty round brackets. Here is the example of creating and calling the function:
function calculateData() {
...
}
calculateData();

2. Що таке оператор return в JavaScript? Як його використовувати в функціях?
The return is Javascript functions is a special instruction, that grabs the code indicated to the right-hand side of this keyword and makes it visible to the code that is calling this function. Once Javascript reads the 'return' keyword and performs the respective code, the execution of the function stops.

3. Що таке параметри та аргументи в функіях та в яких випадках вони використовуються?
Basically, parameters  are variables in the function definition, they are to be indicated in the round brackets next to the name of the function, and arguments are the actual values passed to the function when it is called (to be indicated in the round brackets as well). 

4. Як передати функцію аргументом в іншу функцію?
Function can be passed as an argument into another function. In such case, it is called a call-back function. This call-back functions is being called afterwards, once some kind of routine or action is completed.