let month = prompt('Введіть назву місяця');

switch (month) {
    case 'січень':
        console.log('в січні 31 день');
        break;
    case 'лютий':
        console.log('в лютому 28 днів');
        break;
    case 'березень':
        console.log('в березні 31 день');
        break;
    case 'квітень':
        console.log('в квітні 30 днів');
        break;
    case 'травень':
        console.log('в травні 31 день');
        break;
    case 'червень':
        console.log('в червні 30 днів');
        break;
    case 'липень':
        console.log('в липні 31 день');
        break;
    case 'серпень':
        console.log('в серпні 31 день');
        break;
    case 'вересень':
        console.log('в вересні 30 днів');
        break;
    case 'жовтень':
        console.log('в жовтні 31 день');
        break;
    case 'листопад':
        console.log('в листопаді 30 днів');
        break;
    case 'грудень':
        console.log('в грудні 31 день');
        break;
    default:
        console.log("The entered value is not a month, or contains an error, please make sure you type the name of the month correctly!");
}