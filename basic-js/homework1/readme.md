Теоретичні питання:
1. Як можна оголосити змінну у Javascript?
Variables in Javascript can be declared in 3 different ways. For this purpose, the following keywords may be used: 'var' (old-fashioned option and should not be used these days), 'let' (this method stipulates that the variable can be declared and later it's value can be changed/re-assigned) and 'const' (it means that this variable can be declared only once and cannot be changed afterward).

2. Що таке рядок (string) і як його створити (перерахуйте всі можливі варіанти)?
String is one of the data types in Javascript.
It can be created:
a). let a = String('string-value');
b). let a = 'string-value';
c). let a = "string-value";
d). let a = `string-value`;

3. Як перевірити тип даних змінної в JavaScript?
We can check the data type by using the 'typeof' keyword.
For example:
let a = 'string value';
console.log(typeof a);
console.log(typeof(a));

4. Поясніть чому '1' + 1 = 11.
Because in this case the first value has the string data type and second one has the number data type. In case if at least one of the values involved in the Javascript operation (in our case, it is addition) is not of a number data type, the '+' operator shall consider all operands as those with the string data type and will concatenate the values, simply by putting together the value strings in one word.
