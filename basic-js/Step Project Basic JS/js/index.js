"use strict";

const DATA = [
	{
		"first name": "Олексій",
		"last name": "Петров",
		photo: "./img/trainers/trainer-m1.jpg",
		specialization: "Басейн",
		category: "майстер",
		experience: "8 років",
		description:
			"Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
	},
	{
		"first name": "Марина",
		"last name": "Іванова",
		photo: "./img/trainers/trainer-f1.png",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
	},
	{
		"first name": "Ігор",
		"last name": "Сидоренко",
		photo: "./img/trainers/trainer-m2.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
	},
	{
		"first name": "Тетяна",
		"last name": "Мороз",
		photo: "./img/trainers/trainer-f2.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "10 років",
		description:
			"Тетяна є експертом в бойових мистецтвах. Вона проводить тренування для професіоналів і новачків. Її підхід до навчання допомагає спортсменам досягати високих результатів.",
	},
	{
		"first name": "Сергій",
		"last name": "Коваленко",
		photo: "./img/trainers/trainer-m3.jpg",
		specialization: "Тренажерний зал",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Сергій фокусується на роботі з фітнесом та кардіотренуваннями. Він вдосконалив свої методики протягом багатьох років. Його учні завжди в формі та енергійні.",
	},
	{
		"first name": "Олена",
		"last name": "Лисенко",
		photo: "./img/trainers/trainer-f3.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "4 роки",
		description:
			"Олена спеціалізується на синхронному плаванні. Вона тренує команди різного рівня. Її команди завжди займають призові місця на змаганнях.",
	},
	{
		"first name": "Андрій",
		"last name": "Волков",
		photo: "./img/trainers/trainer-m4.jpg",
		specialization: "Бійцівський клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Андрій має досвід у вивченні різних бойових мистецтв. Він викладає техніку та тактику бою. Його учні здобувають перемоги на міжнародних турнірах.",
	},
	{
		"first name": "Наталія",
		"last name": "Романенко",
		photo: "./img/trainers/trainer-f4.jpg",
		specialization: "Дитячий клуб",
		category: "спеціаліст",
		experience: "3 роки",
		description:
			"Наталія розробила унікальну програму для дітей дошкільного віку. Вона допомагає дітям розвивати фізичні та ментальні навички. Її класи завжди веселі та динамічні.",
	},
	{
		"first name": "Віталій",
		"last name": "Козлов",
		photo: "./img/trainers/trainer-m5.jpg",
		specialization: "Тренажерний зал",
		category: "майстер",
		experience: "10 років",
		description:
			"Віталій спеціалізується на функціональному тренуванні. Він розробив ряд ефективних тренувальних програм. Його клієнти швидко досягають бажаних результатів.",
	},
	{
		"first name": "Юлія",
		"last name": "Кравченко",
		photo: "./img/trainers/trainer-f5.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "4 роки",
		description:
			"Юлія є експертом у водних видах спорту. Вона проводить тренування з аквагімнастики та аеробіки. Її учні демонструють вражаючі показники на змаганнях.",
	},
	{
		"first name": "Олег",
		"last name": "Мельник",
		photo: "./img/trainers/trainer-m6.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "12 років",
		description:
			"Олег є визнаним майстром в бойових мистецтвах. Він тренує чемпіонів різних вагових категорій. Його методики вважаються одними з найефективніших у світі бойових мистецтв.",
	},
	{
		"first name": "Лідія",
		"last name": "Попова",
		photo: "./img/trainers/trainer-f6.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Лідія має великий досвід у роботі з дітьми. Вона організовує різноманітні спортивні ігри та заняття. Її класи завжди допомагають дітям розвивати соціальні навички та командний дух.",
	},
	{
		"first name": "Роман",
		"last name": "Семенов",
		photo: "./img/trainers/trainer-m7.jpg",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Роман є експертом у кросфіту та функціональних тренуваннях. Він розробив власні програми для різних вікових груп. Його учні часто отримують нагороди на фітнес-турнірах.",
	},
	{
		"first name": "Анастасія",
		"last name": "Гончарова",
		photo: "./img/trainers/trainer-f7.jpg",
		specialization: "Басейн",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Анастасія фокусується на водних програмах для здоров'я та фітнесу. Вона проводить тренування для осіб з різним рівнем підготовки. Її учні відзначають покращення здоров'я та благополуччя після занять.",
	},
	{
		"first name": "Валентин",
		"last name": "Ткаченко",
		photo: "./img/trainers/trainer-m8.jpg",
		specialization: "Бійцівський клуб",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Валентин є експертом в таеквондо та кікбоксингу. Він викладає техніку, тактику та стратегію бою. Його учні часто стають чемпіонами на національних та міжнародних змаганнях.",
	},
	{
		"first name": "Лариса",
		"last name": "Петренко",
		photo: "./img/trainers/trainer-f8.jpg",
		specialization: "Дитячий клуб",
		category: "майстер",
		experience: "7 років",
		description:
			"Лариса розробила комплексну програму для розвитку фізичних та інтелектуальних навичок дітей. Вона проводить заняття в ігровій формі. Її методика допомагає дітям стати активними та розумними.",
	},
	{
		"first name": "Олексій",
		"last name": "Петров",
		photo: "./img/trainers/trainer-m9.jpg",
		specialization: "Басейн",
		category: "майстер",
		experience: "11 років",
		description:
			"Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
	},
	{
		"first name": "Марина",
		"last name": "Іванова",
		photo: "./img/trainers/trainer-f9.jpg",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
	},
	{
		"first name": "Ігор",
		"last name": "Сидоренко",
		photo: "./img/trainers/trainer-m10.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
	},
	{
		"first name": "Наталія",
		"last name": "Бондаренко",
		photo: "./img/trainers/trainer-f10.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "8 років",
		description:
			"Наталія є майстром у бойових мистецтвах. Вона вивчала різні техніки та стили, із якими працює зі своїми учнями. Її підхід до навчання відповідає найвищим стандартам.",
	},
	{
		"first name": "Андрій",
		"last name": "Семенов",
		photo: "./img/trainers/trainer-m11.jpg",
		specialization: "Тренажерний зал",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Андрій спеціалізується на функціональному тренуванні. Він розробив власну систему вправ для зміцнення корпусу. Його учні завжди отримують видимі результати.",
	},
	{
		"first name": "Софія",
		"last name": "Мельник",
		photo: "./img/trainers/trainer-f11.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "6 років",
		description:
			"Софія працює з аквагімнастикою. Вона вивчила різні техніки та стили плавання. Її заняття допомагають клієнтам розслабитися та покращити фізичну форму.",
	},
	{
		"first name": "Дмитро",
		"last name": "Ковальчук",
		photo: "./img/trainers/trainer-m12.png",
		specialization: "Дитячий клуб",
		category: "майстер",
		experience: "10 років",
		description:
			"Дмитро спеціалізується на розвитку дитячого спорту. Він розробив унікальну програму для малюків. Його методики забезпечують гармонійний розвиток дітей.",
	},
	{
		"first name": "Олена",
		"last name": "Ткаченко",
		photo: "./img/trainers/trainer-f12.jpg",
		specialization: "Бійцівський клуб",
		category: "спеціаліст",
		experience: "5 років",
		description:
			"Олена є відомим тренером у жіночому бойовому клубі. Вона вивчила різні техніки самооборони. Її підхід дозволяє її ученицям відчувати себе впевнено в будь-яких ситуаціях.",
	},
];

const transformUaFieldsToEng = [
	{
		specialization: 'Тренажерний зал',
		'specialization EN': 'gym',
	},
	{
		specialization: 'Бійцівський клуб',
		'specialization EN': 'fight-club',
	},
	{
		specialization: 'Дитячий клуб',
		'specialization EN': 'kids-club',
	},
	{
		specialization: 'Басейн',
		'specialization EN': 'swimming-pool',
	},
	{
		category: 'майстер',
		'category EN': 'master',
	},
	{
		category: 'спеціаліст',
		'category EN': 'specialist',
	},
	{
		category: 'інструктор',
		'category EN': 'instructor',
	},
];

function addEngProperty() {
	DATA.forEach((dataCard) => {
		const categoryDesiredObject = transformUaFieldsToEng.find(el =>  el.category === dataCard.category);
		const specializationDesiredObject = transformUaFieldsToEng.find(el =>  el.specialization === dataCard.specialization);
		dataCard['category EN'] = categoryDesiredObject['category EN'];
		dataCard['specialization EN'] = specializationDesiredObject['specialization EN'];
	})
}

const sortingSection = document.querySelector('.sorting');
const filterSection = document.querySelector('.sidebar');
const trainerCardTemplate = document.querySelector('#trainer-card');
const trainersCardsContainer = document.querySelector('.trainers-cards__container');
const sortingButtons = document.querySelectorAll('.sorting__btn');
const filtersSubmit = document.querySelector('.filters__submit');
filtersSubmit.addEventListener('click', (e) => {
	e.preventDefault();
})

sortingSection.hidden = false;
filterSection.hidden = false;

function renderTrainerCards(sortedArray) {
	sortedArray.forEach((e) => {
		let clone = trainerCardTemplate.content.cloneNode(true);
		let cardTemplateImage = clone.querySelector('img');
		let cardTemplateTrainerName = clone.querySelector('.trainer__name');
		cardTemplateImage.src = e.photo;
		cardTemplateTrainerName.innerText = `${e["last name"]} ${e["first name"]}`;
		if (!e.id) {
			e.id = DATA.indexOf(e) + 1;
		}
		if (!clone.querySelector('li').getAttribute('rel')) {
			clone.querySelector('li').setAttribute('rel', `${e.id}`);
		}
		trainersCardsContainer.append(clone);
		addEngProperty();
	});
}

document.addEventListener("DOMContentLoaded", () => renderTrainerCards(DATA));

function toggleSortingActiveBtn(target) {
	sortingButtons.forEach((e) => {
		e.classList.remove('sorting__btn--active');
	});
	target.classList.add('sorting__btn--active');
	clearTrainersCardsContainer();
}

function clearTrainersCardsContainer() {
	document.querySelectorAll('.trainer').forEach((e) => {
		e.remove();
	});
}

function closeModal() {
	const modal = document.querySelector('.modal');
	modal.remove();
	document.body.style.overflowY = 'visible';
}

window.addEventListener('click', (e) => {
	const modalTemplate = document.querySelector('#modal-template');
	let clone = modalTemplate.content.cloneNode(true);
	let target = e.target;
	if (target.className === 'trainer__show-more') {
		clone.querySelector('img').src = target.parentNode.querySelector('img').src;
		clone.querySelector('.modal__name').innerText = target.parentNode.querySelector('.trainer__name').innerText;
		let targetObject = DATA.find((e) => {
			return e.id === Number(target.parentNode.getAttribute('rel'));
		})
		clone.querySelector('.modal__point--category').innerText = `Категорія: ${targetObject.category}`;
		clone.querySelector('.modal__point--experience').innerText = `Досвід: ${targetObject.experience}`;
		clone.querySelector('.modal__point--specialization').innerText = `Напрям тренера: ${targetObject.specialization}`;
		clone.querySelector('.modal__text').innerText = `Напрям тренера: ${targetObject.description}`;
		document.body.append(clone);
		document.body.style.overflowY = 'hidden';
	}

	if (target.className === 'modal__close' || target.closest('.modal__close svg')) {
		closeModal();
	}

	if (target.innerText.toLowerCase() === 'за замовчуванням') {
		toggleSortingActiveBtn(target);
		if (filteredTrainers.length === 24) {
			renderTrainerCards(DATA.sort((a, b) => a.id > b.id ? 1 : -1));
		} else if (filteredTrainers.length !== 24) {
			renderTrainerCards(filteredTrainers.sort((a, b) => a.id > b.id ? 1 : -1));
		}
	}

	if (target.innerText.toLowerCase() === 'за прізвищем') {
		toggleSortingActiveBtn(target);
		if (filteredTrainers.length === 24) {
			renderTrainerCards(DATA.sort((a, b) => a["last name"].localeCompare(b["last name"])));
		} else if (filteredTrainers.length !== 24) {
			renderTrainerCards(filteredTrainers.sort((a, b) => a["last name"].localeCompare(b["last name"])));
		}
	}
	if (target.innerText.toLowerCase() === 'за досвідом') {
		toggleSortingActiveBtn(target);
		if (filteredTrainers.length === 24) {
			renderTrainerCards(DATA.sort((a, b) => (+a.experience.split(' ')[0]) > (+b.experience.split(' ')[0]) ? -1 : 1));
		} else if (filteredTrainers.length !== 24) {
			renderTrainerCards(filteredTrainers.sort((a, b) => (+a.experience.split(' ')[0]) > (+b.experience.split(' ')[0]) ? -1 : 1));
		}
	}
});

const showBtn = document.querySelector('.filters__submit');
let filteredTrainers = [...DATA];

function filterTrainers() {
	const directionInput = document.querySelector('input[name="direction"]:checked');
	const categoryInput = document.querySelector('input[name="category"]:checked');

	filteredTrainers = DATA.filter((trainer) => {
		if (directionInput.value === 'all' && categoryInput.value === 'all') {
			clearTrainersCardsContainer();
			return true;
		} else if (directionInput.value !== 'all' && categoryInput.value === 'all') {
			// return trainer.category === "спеціаліст";
			clearTrainersCardsContainer();
			return trainer['specialization EN'] === directionInput.id;
		} else if (directionInput.value === 'all' && categoryInput.value !== 'all') {
			clearTrainersCardsContainer();
			return trainer['category EN'] === categoryInput.id;
		} else {
			clearTrainersCardsContainer();
			return trainer['specialization EN'] === directionInput.id && trainer['category EN'] === categoryInput.id;
		}
	});

	let sortActive = document.querySelector('.sorting__btn--active');
	if (sortActive.innerText.toLowerCase() === 'за замовчуванням') {
		renderTrainerCards(filteredTrainers.sort((a, b) => a.id > b.id ? 1 : -1));
	}

	if (sortActive.innerText.toLowerCase() === 'за прізвищем') {
		renderTrainerCards(filteredTrainers.sort((a, b) => a["last name"].localeCompare(b["last name"])));
	}

	if (sortActive.innerText.toLowerCase() === 'за досвідом') {
		renderTrainerCards(filteredTrainers.sort((a, b) => (+a.experience.split(' ')[0]) > (+b.experience.split(' ')[0]) ? -1 : 1));
	}
}

showBtn.addEventListener('click', (e) => {
	e.preventDefault();
	filterTrainers();
});