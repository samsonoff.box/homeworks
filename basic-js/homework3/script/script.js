let age = prompt('Please enter your age');

if (isNaN(+age)) {
    alert('Error! This value should be a number, please enter correct age!');
}

if (age < 12 ) {
    alert('Your age is less than 12 years, you are a child! Access denied!');
} else if (age >= 12 && age < 18) {
    alert('Your age is less than 18 years, you are a teenager! Access denied!');
} else {
    alert('You are an adult, welcome to our website!');
}