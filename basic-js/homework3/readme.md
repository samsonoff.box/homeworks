Теоретичні питання
1. Що таке логічний оператор?
Logical operators is Javascript are used to create conditions and conditions checks in order to filter or validate values/variables. They help us to process necessary scenarios (for example, we want to validate only those data entered by user that meet our criteria, in alert form, as an example, we only want to accept values entered as numbers and within certain range).


2. Які логічні оператори є в JavaScript і які їх символи?
Most common logical operators in Javascript are:
|| - logical OR;
||= - logical OR assignment;
&& - logical AND;
&&= - logical AND assignment;
! - logical NOT;
?? - nullish coalescing operator;
??= - nullish assignment operator.