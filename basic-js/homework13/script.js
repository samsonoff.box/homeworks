/* Теоретичні питання
1. Яке призначення методу event.preventDefault() у JavaScript?
The preventDefault() method of the Event interface tells the user agent that if the event does not get explicitly handled, its default action should not be taken as it normally would be.

2. В чому сенс прийому делегування подій?
The idea is that if we have a lot of elements handled in a similar way, then instead of assigning a handler to each of them – we put a single handler on their common ancestor.

In the handler we get event.target to see where the event actually happened and handle it.

3. Які ви знаєте основні події документу та вікна браузера?
DOMContentLoaded – the browser fully loaded HTML, and the DOM tree is built, but external resources like pictures <img> and stylesheets may not yet have loaded.
load – not only HTML is loaded, but also all the external resources: images, styles etc.
beforeunload/unload – the user is leaving the page.

Практичне завдання:

Реалізувати перемикання вкладок (таби) на чистому Javascript.

Технічні вимоги:

- У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
- Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
- Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.

 Умови:
 - При реалізації обов'язково використовуйте прийом делегування подій (на весь скрипт обробник подій повинен бути один).
*/

// const myTabs = document.querySelectorAll('.tabs-title');
// const texts = document.querySelectorAll('.tabs-content li');
//
// for (let i = 0; i < myTabs.length; i++) {
//     console.log('i', myTabs[i].textContent);
//     for (let j = 0; j < texts.length; j++) {
//         console.log('j', texts[j]);
//         texts[i].setAttribute('data-name', myTabs[i].textContent);
//     }
// }

const tabsUl = document.querySelector('.tabs');
const tabs = document.querySelector('.tabs').children;
const tabsContent = document.querySelector('.tabs-content').children;

for (const tabsContentElement of tabsContent) {
    tabsContentElement.style.display = 'none';
}
tabsContent[0].style.display = 'block';

tabsUl.addEventListener('click', makeTabActive);

function makeTabActive(e) {
    for (let i = 0; i < tabs.length; i++) {
        tabs[i].classList.remove('active');
        e.target.classList.add('active');
        tabs[i].setAttribute('data-id', `tab${i+1}`);
        tabsContent[i].setAttribute('data-id', `tab${i+1}`);
        if (tabsContent[i].dataset.id === e.target.dataset.id) {
            tabsContent[i].style.display = 'block';
        } else {
            tabsContent[i].style.display = 'none';
        }
    }
}