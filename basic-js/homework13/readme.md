Теоретичні питання
1. Яке призначення методу event.preventDefault() у JavaScript?
   The preventDefault() method of the Event interface tells the user agent that if the event does not get explicitly handled, its default action should not be taken as it normally would be.

2. В чому сенс прийому делегування подій?
   The idea is that if we have a lot of elements handled in a similar way, then instead of assigning a handler to each of them – we put a single handler on their common ancestor.
In the handler we get event.target to see where the event actually happened and handle it.

3. Які ви знаєте основні події документу та вікна браузера?
   DOMContentLoaded – the browser fully loaded HTML, and the DOM tree is built, but external resources like pictures <img> and stylesheets may not yet have loaded.
   load – not only HTML is loaded, but also all the external resources: images, styles etc.
   beforeunload/unload – the user is leaving the page.