// Теоретичні питання
// 1. Як можна визначити, яку саме клавішу клавіатури натиснув користувач?
// We can add event listener to the Window global object (or Document, better use Window, though), 'keydown' listener type, and through the listener get the event object which contains all the information regarding the element that user chose to click and/or button that he pressed/entered, by searching for 'key'/'code' property.
//
// 2. Яка різниця між event.code() та event.key()?
// The 'key' property of the event object allows to get the character, while the 'code' property of the event object allows to get the 'physical' code key. For instance, the same 'z' key can be pressed with or without Shift. That gives us two different characters: lowercase 'z' and uppercase Z. The event.key is exactly the character, and it will be different. But event.code is the same.
//
// 3. Які три події клавіатури існує та яка між ними відмінність?
// We have 3 types of keyboard events: 'keyup' (a key has been released), 'keydown' (a key has been pressed) and 'keypress' (a key that normally produces a character value has been pressed. This event was highly device-dependent and is obsolete. The general advice is not to use this anymore).