// Теоритичні питання:
// 1. В чому відмінність між setInterval та setTimeout?
// setTimeout allows us to run a function once after the interval of time. Whereas, setInterval allows us to run a function repeatedly, starting after the interval of time, then repeating continuously at that interval.
//
// 2. Чи можна стверджувати, що функції в setInterval та setTimeout будуть виконані рівно через той проміжок часу, який ви вказали?
// No, the timing will not be exactly as indicated by user. This timing will be affected by the call stack completion time.
//
// 3. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?
// For this purpose, we can use clearInterval/clearTimeout method.