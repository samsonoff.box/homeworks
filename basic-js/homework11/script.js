// Теоретичні питання
// 1. Що таке події в JavaScript і для чого вони використовуються?
// Events in Javascript are things that "occur" to the HTML elements, or set of elements. As a programmer, we have to process these events and react correspondingly. For example, our user might click one of our elements, or resize the window, or press a key on a keyboard, or submit a form. We have to process the event, get the target element, and initiate the corresponding action (warning message, submission of the form data, add selected product to the cart etc.)

// 2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
// There is a range of mouse events in Javascript. We have mousedown/mouseup (mouse button is clicked/released over an element), mouseover/mouseout (mouse pointer comes over/out from an element), mousemove (every mouse move over an element triggers), click (click on an element), double-click (double-click on an element), context menu (triggers when the right mouse button is pressed).

// 3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?
// The context menu event fires when the user attempts to open a context menu. This event is typically triggered by clicking the right mouse button, or by pressing the context menu key.

// Практичні завдання
//  1. Додати новий абзац по кліку на кнопку:
//   По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">

let button = document.getElementById('btn-click');
let newElementP = document.createElement('p');
newElementP.innerText = 'New Paragraph';
let sectionElement = document.getElementById('content');
button.addEventListener('click', addNewElement);

function addNewElement() {
    sectionElement.append(newElementP);
}

// 2. Додати новий елемент форми із атрибутами:
// Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
//  По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.

let newButton = document.createElement('button');
newButton.setAttribute('id', 'btn-input-create');
newButton.innerText = 'New Button';
sectionElement.append(newButton);
let br = document.createElement('br');
let newInput = document.createElement('input');

newButton.addEventListener('click', createNewInput);

function createNewInput() {
    newInput.setAttribute('type', 'text');
    newInput.setAttribute('placeholder', 'some text');
    newInput.setAttribute('name', 'my-input');
    sectionElement.append(br);
    sectionElement.append(newInput);
}