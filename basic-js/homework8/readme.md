Теоретичні питання
1. Опишіть своїми словами як працює метод forEach.
ForEach method is a loop that is used for arrays. Basically, it iterates every element of the array and returns you the elements one by one.

2. Які методи для роботи з масивом мутують існуючий масив, а які повертають новий масив? Наведіть приклади.
The methods that modify the existing array are: push (adds new element at the end of the array), unshift (adds new element at the beginning of the array), pop (removes the last element of the array), shift (removes the first element of the array).
The methods that return new array, are: splice method (returns array of deleted elements from the initial array), concat method (concatenates two or more arrays and eventually returns new array), slice method (creates a copy o partially copies the existing array and returns a new array). 

3. Як можна перевірити, що та чи інша змінна є масивом?
We can use Array.isArray() construction for this purpose. For example:
let userData = [1, 2, 3];
let userData2 = 'string';

console.log(Array.isArray(userData));
console.log(Array.isArray(userData2));

The 1st console.log shall return 'true', the 2nd one - 'false'.

4. В яких випадках краще використовувати метод map(), а в яких forEach()?
We want to use Map method when need to return new array, whereas ForEach method only modifies our initial array, or returns values from our initial array. Apart from this, Map method is operating faster, normally (it may depend on a browser, but usually ForEach method is slower).