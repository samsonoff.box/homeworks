// Task 1: Створіть масив з рядків "travel", "hello", "eat", "ski", "lift" та обчисліть кількість рядків з довжиною більше за 3 символи. Вивести це число в консоль.

let myArray = ['travel', 'hello', 'eat', 'ski', 'lift'];
let filteredArray = [];

for (const string of myArray) {
    if (string.length > 3) {
        filteredArray.push(string);
    }
}
console.log(filteredArray.length);

// Task 2: Створіть масив із 4 об'єктів, які містять інформацію про людей: {name: "Іван", age: 25, sex: "чоловіча"}. Наповніть різними даними. Відфільтруйте його, щоб отримати тільки об'єкти зі sex "чоловіча".
//
// Відфільтрований масив виведіть в консоль.

let arrayOfObjects = [
    { name: "Ivan", age: 25, sex: "male" },
    { name: "Jessica", age: 37, sex: "female" },
    { name: "Steven", age: 18, sex: "male" },
    { name: "Caroline", age: 43, sex: "female" },
];

const filteredArrayOfObjects = arrayOfObjects.filter(e => { return e.sex === 'male'});

console.log(filteredArrayOfObjects);

// Task 3: Реалізувати функцію фільтру масиву за вказаним типом даних. (Опціональне завдання)

let newArr = [];

function filterBy(userArray, dataType) {
    userArray.forEach(e => {
        if (typeof e !== dataType) {
            newArr.push(e);
        }
    })
    return newArr;
}

console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));