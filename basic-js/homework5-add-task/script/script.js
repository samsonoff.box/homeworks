let userNumber;
let array = [];
let result;

do {
    userNumber = prompt('Please enter a number and get its factorial');
} while (isNaN(userNumber) || userNumber === null || userNumber === '' || userNumber.trim().length === 0 || userNumber <= 0);

function factorial() {
    for (let i = 1; i <= userNumber; i++) {
        array.push(i);
        result = array.reduce((accumulator, currentValue) => accumulator * currentValue);
    }
    alert(`The factorial of the entered number is ${result}`);
}

factorial();