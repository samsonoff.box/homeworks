// Task 1 (isPalindrome function)
function isPalindrome(str) {
    return str === str.split('').reverse().join('');
}

console.log(isPalindrome('kayak'));

// Task 2 (string length validation)
function checkedString (string) {
    return string.length <= 20;
}

console.log(checkedString('helloooooooooooooooo'));

// Task 3 (calculation of full years of our user)

let userDateOfBirth = new Date(prompt('Please indicate your date of birth', '1988, 09, 31'));

let now = new Date();
let today = new Date(now.getFullYear(), now.getMonth(), now.getDate()); // Current date without indication of hours/minutes/seconds
let dateOfBirthNow = new Date(today.getFullYear(), userDateOfBirth.getMonth(), userDateOfBirth.getDate()); // Birthday in current year
let userAge = today.getFullYear() - userDateOfBirth.getFullYear();

function countFullYears(age) {
    if (today < dateOfBirthNow) {
        userAge = userAge - 1;
    } // If the birthday in current year is still to come, we deduct 1 year from "userAge" variable
    return userAge;
}

console.log(countFullYears(userDateOfBirth));