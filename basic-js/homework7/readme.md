Теоретичні питання
1. Як можна створити рядок у JavaScript?
A string in Javascript can be created by declaring value in single quotes, double quotes, backslashes and using new String constructor.

2. Яка різниця між одинарними (''), подвійними ("") та зворотніми (``) лапками в JavaScript?
Single quotes and double quotes are basically the same thing, it is commonly agreed that single quotes are used in Javascript files and double quotes are used in HTML files, whereas backslashes are used in cases when you need to use your variables within these backslashes content. Only backslashes method will allow you to put your contents in multiple rows. 

3. Як перевірити, чи два рядки рівні між собою?
For this purpose, you will have to use a strict equality sign (===) or a loose equality sign (==). 

4. Що повертає Date.now()?
It returns a quantity of milliseconds that have passed from January, 1, 1970 till the moment of calling this function/method. January, 1, 1970 being the equivalent to the UNIX epoch.

5. Чим відрізняється Date.now() від new Date()?
These two methods are similar and basically do the same thing, but Date.now() method does not create intermediate Date object and thus operates faster and does not overload the garbage collector. 