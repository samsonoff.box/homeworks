Теоретичні питання
1. Опишіть своїми словами, що таке метод об'єкту
A method in an object is a regular function that can be used within this object or beyond the object. An example of it can be as follows (method "sayHi"):
const obj = {
    name: 'Ivan',
    lastName: 'Samsonov',
    sayHi () {
        return 'hello'
    }
}

2. Який тип даних може мати значення властивості об'єкта?
I didn't find this exact information on the Internet, but after having tried to input all existing data types inside the object, I didn't get any error, therefore, all data types are allowed in the JS object.

3. Об'єкт це посилальний тип даних. Що означає це поняття?
Primitive data type is the one that stores the data inside itself, the reference data type, on the other hand (like an object) is the one that stores the link for this data, for example when assign an object to a variable, it only assigns the link to this variable, but not the data.