// Теоретичні питання
// 1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?
// Creating a new DOM-element can be done using the following structure:
// document.createElement('div);
// Pushing this element to the DOM can be done as follows:
// Element.append/prepend/after/before(newElement)
// Second option: Element.insertAdjacentElement('beforebegin'/'afterbegin'/'beforeend'/'afterend', newElement);
//
// 2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.
// First step: we have to find the necessary element that needs to be deleted -
// let eLementNavigation = document.querySelector('.navigation');
// eLementNavigation.remove();
//
// 3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?
// In this regard, we have two options:
// a). Element.append/prepend/after/before(newElement)
// append - puts data inside an element at last index;
// prepend - puts data inside an element at first index;
// after - puts the element after the element;
// before - puts the element before the element;
// b). Element.insertAdjacentElement('beforebegin'/'afterbegin'/'beforeend'/'afterend', newElement);
// 'beforebegin': before the target element itself;
// 'afterbegin': just inside the target element, before its first child;
// 'beforeend': just inside the target element, after its last child;
// 'afterend': after the target element itself.

// Практичні завдання
//  1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.

const footer = document.querySelector('footer');
const newElementA = document.createElement('a');
newElementA.textContent = 'Learn More';
newElementA.setAttribute('href', '#');
footer.append(newElementA);

// 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
// Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
// Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
// Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
// Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.

const mainTag = document.querySelector('main');
const newElementSelect = document.createElement('select');
newElementSelect.setAttribute('id', 'rating');
mainTag.prepend(newElementSelect);

const newElementOption1 = document.createElement('option');
const newElementOption2 = newElementOption1.cloneNode();
const newElementOption3 = newElementOption1.cloneNode();
const newElementOption4 = newElementOption1.cloneNode();

const arrayOfOptions = [newElementOption1, newElementOption2, newElementOption3, newElementOption4];

for (let i = 4; i > 0; i--) {
    arrayOfOptions[i-1].setAttribute('value', `${[i]}`);
    arrayOfOptions[i-1].textContent = `${[i] + ' Stars'}`;
    newElementSelect.append(arrayOfOptions[i-1]);
}