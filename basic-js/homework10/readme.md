// Теоретичні питання
// 1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?
// Creating a new DOM-element can be done using the following structure:
// document.createElement('div);
// Pushing this element to the DOM can be done as follows:
// Element.append/prepend/after/before(newElement)
// Second option: Element.insertAdjacentElement('beforebegin'/'afterbegin'/'beforeend'/'afterend', newElement);

// 2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.
// First step: we have to find the necessary element that needs to be deleted -
// let eLementNavigation = document.querySelector('.navigation');
// eLementNavigation.remove();

// 3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?
// In this regard, we have two options:
// a). Element.append/prepend/after/before(newElement)
// append - puts data inside an element at last index;
// prepend - puts data inside an element at first index;
// after - puts the element after the element;
// before - puts the element before the element;
// b). Element.insertAdjacentElement('beforebegin'/'afterbegin'/'beforeend'/'afterend', newElement);
// 'beforebegin': before the target element itself;
// 'afterbegin': just inside the target element, before its first child;
// 'beforeend': just inside the target element, after its last child;
// 'afterend': after the target element itself.