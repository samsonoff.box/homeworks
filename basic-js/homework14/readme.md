// Теоритичні питання:
//     1. В чому полягає відмінність localStorage і sessionStorage?
//     The difference is that while data in localStorage doesn't expire, data in sessionStorage is cleared when the page session ends.
//
//     2. Які аспекти безпеки слід враховувати при збереженні чутливої інформації, такої як паролі, за допомогою localStorage чи sessionStorage?
//     To make this process more secure, we should use safer authentication methods, such as access tokens, use HTTPS (this will provide encryption of data that is being exchanged between the client and the server), and we can also try using browser settings for access control (in regard to localStorage and sessionStorage), when it's possible.
//
//     3. Що відбувається з даними, збереженими в sessionStorage, коли завершується сеанс браузера?
//     They are automatically being cleared, once the user closes corresponding tab.