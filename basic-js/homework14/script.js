// Теоритичні питання:
//     1. В чому полягає відмінність localStorage і sessionStorage?
//     The difference is that while data in localStorage doesn't expire, data in sessionStorage is cleared when the page session ends.
//
//     2. Які аспекти безпеки слід враховувати при збереженні чутливої інформації, такої як паролі, за допомогою localStorage чи sessionStorage?
//      To make this process more secure, we should use safer authentication methods, such as access tokens, use HTTPS (this will provide encryption of data that is being exchanged between the client and the server), and we can also try using browser settings for access control (in regard to localStorage and sessionStorage), when it's possible.
//
//     3. Що відбувається з даними, збереженими в sessionStorage, коли завершується сеанс браузера?
// They are automatically being cleared, once the user closes corresponding tab.

const body = document.body;
const button = document.querySelector('.theme-toggle-btn');
let path = document.querySelectorAll('path');
let mode = 'light';

if (!localStorage.getItem('mode')) {
    localStorage.setItem('mode', mode);
} else {
    mode = localStorage.getItem('mode');
}

if (mode === 'dark') {
    changeToggle(mode);
}

button.addEventListener('click', () => {
    if (mode === 'light') {
        changeToggle('dark');
    } else if (mode === 'dark') {
        changeToggle('light');
    }
    localStorage.setItem('mode', mode);
});

function changeToggle(newMode) {
    if (newMode === 'dark') {
        body.className = 'dark-mode';
        mode = 'dark';
        for (const elem of path) {
            elem.classList.toggle('path-white');
        }
    } else if (newMode === 'light') {
        body.className = 'light-mode';
        mode = 'light';
        for (const elem of path) {
            elem.classList.toggle('path-white');
        }
    }
}