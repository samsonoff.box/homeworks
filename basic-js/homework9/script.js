/*
Теоретичні питання
1. Опишіть своїми словами що таке Document Object Model (DOM)
The HTML Document Object Model is a standard object model and programming interface for HTML. It defines the properties of all HTML elements and the methods to access them. The Document Object Model is a standard for how to get, change, add, or delete HTML elements.

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
The InnerHTML property returns the text content of the element, including all spacing and inner HTML tags. The InnerText property returns just the text content of the element and all its children.

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
We have several options to find an HTML element: querySelector (finds first element with specified selector), querySelectorAll (finds all elements with specified selector), getElementById (finds the element with unique id), getElementsByName (finds the element with respective name attribute), getElementsByTagName (finds elements based on the tag name), getElementsByClassName (finds all elements with the specified class name).
Comparing all these methods, the best one would be the one that fits your needs the most, depending on the output that you are looking for. The easiest way to find a DOM element would be getElementById, but it's not always a panacea if you need to get all elements with a specific class/tag name, for example.

4. Яка різниця між nodeList та HTMLCollection?
HTML Collection and NodeList are collections that are very similar to arrays. An HTML Collection is a collection of document elements. HTML Collection items can be accessed by their name, id or index number. A NodeList is a collection of document nodes (element nodes, attribute nodes and text nodes). NodeList items can only be accessed by their index number. We also have to bear in mind that NodeList model DOES NOT reflect dynamical changes in its contents. For example, if we add an element into HTML Collection and log its length, it will update automatically. Whereas the NodeList will NOT indicate any changes, the length will remain without changes.

Практичні завдання
 1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
 Використайте 2 способи для пошуку елементів.
 Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).
<<<<<<< HEAD

=======
>>>>>>> b7004d4accd9bcf73b80e0d759c7db73c7c03628
 */

let objectWithClassFeature = document.getElementsByClassName('feature');
console.log(objectWithClassFeature);
for (let i = 0; i < objectWithClassFeature.length; i++) {
    objectWithClassFeature[i].style.textAlign = 'center';
}

// Second option
// let objectWithClassFeature2 = document.querySelectorAll('.feature');
// console.log(objectWithClassFeature2);
// for (let i = 0; i < objectWithClassFeature2.length; i++) {
//     objectWithClassFeature2[i].style.backgroundColor = "red";
//     objectWithClassFeature2[i].style.textAlign = 'left';
// }

// 2. Змініть текст усіх елементів h2 на "Awesome feature".

let h2 = document.getElementsByTagName('h2');
for (let i = 0; i < h2.length; i++) {
    h2[i].innerHTML = 'Awesome feature';
}

// 3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".

let feature_title = document.getElementsByClassName('feature-title');
for (let i = 0; i < feature_title.length; i++) {
    feature_title[i].innerHTML += "!";
}