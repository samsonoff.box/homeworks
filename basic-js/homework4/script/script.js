let number1;

do {
    number1 = prompt('Please indicate a number');
} while (isNaN(number1) || number1 === '' || number1.trim().length === 0);

let number2;
do {
    number2 = prompt('Please indicate another number');
} while (isNaN(number2) || number2 === '' || number2.trim().length === 0);

for (let i = 0; i < 1; i++) {
    if (+number1 >= +number2) {
        alert(`First number is ${number1} and second number is ${number2}`);
    } else if (+number2 >= +number1) {
        alert(`First number is ${number1} and second number is ${number2}`);
    }
}