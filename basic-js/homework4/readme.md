Теоретичні питання
1. Що таке цикл в програмуванні?
In computer programming, a loop is a sequence of instructions that are continually repeated until a certain condition is reached.

2. Які види циклів є в JavaScript і які їх ключові слова?
In computer programming, we have different types of loops. The two main loops are "do...while" and "for". The first one has the following syntax: "do {algorythms} while (condition is true)"
"For" loop normally has the following syntax: for (i = 0; i < 3; i++). The arguments may vary, of course, depending on the needs.

3. Чим відрізняється цикл do while від while?
Loop "while" checks the condition before the code/instruction is done. "Do-while" loop, on the other hand, checks the conditions AFTER the code/instruction is done. The instruction of the "while" loop can not be done unless the condition is true. "Do-while" loop at least will be run once.