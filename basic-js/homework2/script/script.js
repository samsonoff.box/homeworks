// Task 1
let username = 'Ivan';
let password = 'secret';
let userPassword = prompt('Please enter your password');
console.log(password === userPassword);

// Task 2
let x = 5;
let y = 3;

// All alerts in 1:
alert(`Addition: ${x+y} + subtraction: ${x-y} + multiplication: ${x*y} + division: ${x/y}`);

// Separate alerts:
// alert(x+y);
// alert(x-y);
// alert(x*y);
// alert(x/y);