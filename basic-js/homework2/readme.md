Теоретичні питання
1. Що таке оператори в JavaScript і які їхні типи ви знаєте?
Operators are special symbols that are used for different operations with operands (values, variables, functions etc.). Basic JS operators are: addition, subtraction, multiplication, division, assignment, equal to, not equal to, greater than, less than, greater or equals, less or equals, remainder operator, logical AND, logical OR, etc. 

2. Для чого використовуються оператори порівняння в JavaScript? Наведіть приклади таких операторів.
Comparison operators in Javascript are used to compare different values or data types. Or, another popular case is to execute check for a null/undefined value.
Most popular comparison operators are == (non-strict equality), === (strict equality), != (non-strict inequality), !== (strict inequality), > (greater than), < (less than), >= (greater or equal to), <= (less than or equal to).

3. Що таке операції присвоєння в JavaScript? Наведіть кілька прикладів операцій присвоєння.
The assignment operations are those that are used to save certain data/variables/values/objects/etc. into a variable on the left-hand side. In other words, in the following example, we assign number 5 to the variable named "a".
let a = 5;
The values (or data) indicated to the right-hand side of the equal sign, are being assigned to the variable indicated to the left-hand side of the equal sign.
Another example of the assignment operation would be as follows:
let a = b + c;
Another, a little more advanced way of using assignment operations is as follows:
a += b, which means a = a + b;
a -= b, which means a = a - b;
a *= b, which means a = a * b;
a /= b, which means a = a / b;