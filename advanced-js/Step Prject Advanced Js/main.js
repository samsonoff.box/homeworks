import Modal from './modal.js';
import Visit from './visit.js';
import VisitDentist from './VisitDentist.js';
import VisitCardiologist from './VisitCardiologist.js';
import VisitTherapist from './VisitTherapist.js';

console.log('hello');

// Код для керування логікою сторінки
document.addEventListener('DOMContentLoaded', () => {
    const authButton = document.getElementById('authButton');
    const modalOverlay = document.getElementById('modalOverlay');
    const authModal = new Modal(document.getElementById('authModal'), modalOverlay);
    const visitModal = new Modal(document.getElementById('visitModal'), modalOverlay);

    authButton.addEventListener('click', () => authModal.open());

    document.getElementById('closeAuthModal').addEventListener('click', () => authModal.close());
    document.getElementById('loginButton').addEventListener('click', handleLogin);

    document.getElementById('closeVisitModal').addEventListener('click', () => visitModal.close());
    document.getElementById('createVisitButton').addEventListener('click', createVisit);

    function handleLogin() {
        // Логіка для авторизації
        // Після успішної авторизації:
        authButton.innerText = 'Створити візит';
        authButton.removeEventListener('click', authModal.open);
        authButton.addEventListener('click', () => visitModal.open());
        authModal.close();
    }

    function createVisit() {
        // Логіка для створення візиту
    }
});

// Для взаємодії з сервером через AJAX можна використовувати fetch або бібліотеку axios. Це дозволить динамічно додавати, оновлювати та видаляти картки візитів без перезавантаження сторінки.
// async function fetchVisits() {
//     try {
//         const response = await fetch('https://ajax.test-danit.com/api/cards');
//         const data = await response.json();
//         // Обробка даних та оновлення DOM
//     } catch (error) {
//         console.error('Error fetching visits:', error);
//     }
// }

