export default class Modal {
    constructor(modalElement, overlayElement) {
        this.modalElement = modalElement;
        this.overlayElement = overlayElement;
    }

    open() {
        this.overlayElement.classList.remove('hidden');
    }

    close() {
        this.overlayElement.classList.add('hidden');
    }
}