export default class Visit {
    constructor(fullName, visitPurpose, description, urgency) {
        this.fullName = fullName;
        this.visitPurpose = visitPurpose;
        this.description = description;
        this.urgency = urgency;
    }

    render() {
        // Метод для рендерингу картки візиту
    }
}