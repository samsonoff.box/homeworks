// const API = 'https://ajax.test-danit.com/api/swapi/films';
//
// fetch(API)
// .then(res => res.json())
// .then((data) => {
//     data.forEach(({episodeId, name, openingCrawl}) => {
//         const ul = document.createElement('ul');
//         const li = document.createElement('li');
//         const li2 = document.createElement('li');
//         li.innerText = `Episode ${episodeId}: ${name}`;
//         li.classList.add('li');
//         li2.innerText = `${openingCrawl}`;
//         ul.style.backgroundColor = 'lightyellow';
//         ul.append(li)
//         ul.append(li2)
//         document.body.append(ul);
//     })
// })



// fetch('https://ajax.test-danit.com/api/swapi/people/')
//     .then(response => response.json())
//     .then(data => {
//         data.forEach((e) => {
//             console.log(e.name)
//         })
//     })

// fetch('https://ajax.test-danit.com/api/swapi/films/')
//     .then(response => response.json())
//     .then(data => {
//         data.forEach((e) => {
//             // console.log(e.characters)
//             const characters = e.characters;
//             console.log(characters)
//         })
//     })




async function fetchFilms() {
    try {
        const response = await fetch('https://ajax.test-danit.com/api/swapi/films');
        const films = await response.json();

        const filmsContainer = document.getElementById('films-container');

        films.forEach(film => {
            const filmDiv = document.createElement('div');
            filmDiv.id = `film-${film.episodeId}`;
            filmDiv.innerHTML = `
                        <h2>Episode ${film.episodeId}: ${film.name}</h2>
                        <p>${film.openingCrawl}</p>
                        <h3>Characters:</h3>
                        <ul id="characters-${film.episodeId}">Loading characters...</ul>
                    `;
            filmsContainer.appendChild(filmDiv);

            fetchCharacters(film.characters, film.episodeId);
        });
    } catch (error) {
        console.error('Failed to fetch films:', error);
    }
}



async function fetchCharacters(charactersUrls, episodeId) {
    try {
        const characterPromises = charactersUrls.map(url => fetch(url).then(response => response.json()));
        const characters = await Promise.all(characterPromises);

        const charactersList = document.getElementById(`characters-${episodeId}`);
        charactersList.innerHTML = '';

        characters.forEach(character => {
            const listItem = document.createElement('li');
            listItem.textContent = character.name;
            charactersList.appendChild(listItem);

        });
    } catch (error) {
        console.error('Failed to fetch characters:', error);
    }
}

fetchFilms();