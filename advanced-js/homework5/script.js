document.addEventListener('DOMContentLoaded', () => {
    const loader = document.getElementById('loader');
    const postsContainer = document.getElementById('postsContainer');
    const addPostButton = document.getElementById('addPostButton');
    const modal = document.getElementById('modal');
    const closeModalButton = document.querySelector('.close-button');
    const submitPostButton = document.getElementById('submitPostButton');

    class Card {
        constructor(post, user) {
            this.post = post;
            this.user = user;
        }

        createCardElement() {
            const card = document.createElement('div');
            card.classList.add('card');
            card.innerHTML = `
                <h3>${this.post.title}</h3>
                <p>${this.post.body}</p>
                <p>Posted by: ${this.user.name} (${this.user.email})</p>
                <button class="delete-button">Delete</button>
                <button class="edit-button">Edit</button>
            `;
            card.querySelector('.delete-button').addEventListener('click', () => this.deleteCard(card));
            card.querySelector('.edit-button').addEventListener('click', () => this.editCard(card));
            return card;
        }

        deleteCard(cardElement) {
            fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, { method: 'DELETE' })
                .then(response => {
                    if (response.ok) {
                        cardElement.remove();
                    } else {
                        console.error('Failed to delete the post.');
                    }
                });
        }

        editCard(cardElement) {
            const newTitle = prompt('Edit title', this.post.title);
            const newBody = prompt('Edit body', this.post.body);
            if (newTitle && newBody) {
                fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
                    method: 'PUT',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({ ...this.post, title: newTitle, body: newBody })
                })
                    .then(response => response.json())
                    .then(updatedPost => {
                        this.post = updatedPost;
                        cardElement.querySelector('h3').innerText = updatedPost.title;
                        cardElement.querySelector('p').innerText = updatedPost.body;
                    });
            }
        }
    }

    async function fetchData() {
        loader.style.display = 'block';
        const usersResponse = await fetch('https://ajax.test-danit.com/api/json/users');
        const postsResponse = await fetch('https://ajax.test-danit.com/api/json/posts');
        const users = await usersResponse.json();
        const posts = await postsResponse.json();
        loader.style.display = 'none';
        displayPosts(posts, users);
    }

    function displayPosts(posts, users) {
        postsContainer.innerHTML = '';
        posts.forEach(post => {
            const user = users.find(user => user.id === post.userId);
            const card = new Card(post, user);
            postsContainer.appendChild(card.createCardElement());
        });
    }

    addPostButton.addEventListener('click', () => {
        modal.style.display = 'block';
    });

    closeModalButton.addEventListener('click', () => {
        modal.style.display = 'none';
    });

    submitPostButton.addEventListener('click', () => {
        const title = document.getElementById('newPostTitle').value;
        const body = document.getElementById('newPostBody').value;
        if (title && body) {
            fetch('https://ajax.test-danit.com/api/json/posts', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ title, body, userId: 1 })
            })
                .then(response => response.json())
                .then(newPost => {
                    fetchData(); // To reload posts with the new one added
                    modal.style.display = 'none';
                });
        }
    });

    fetchData();
});