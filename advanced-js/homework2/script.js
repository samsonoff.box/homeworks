const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const root = document.getElementById('root');
const ul = document.createElement('ul');

books.forEach((book, index) => {
    try {
        if (!book.author) {
            throw new Error(`Missing 'author' property in book object at index ${index}`);
        }
        if (!book.price) {
            throw new Error(`Missing 'price' property in book object at index ${index}`);
        }
        if (!book.name) {
            throw new Error(`Missing 'name' property in book object at index ${index}`);
        }

        if (book.hasOwnProperty('author') && book.hasOwnProperty('name') && book.hasOwnProperty('price')) {
            const li = document.createElement('li');
            li.textContent = `${book.author} - "${book.name}" (Ціна: ${book.price} грн)`;
            ul.appendChild(li);
        }

    } catch (error) {
        console.log(error.message);
    }
})

root.appendChild(ul);