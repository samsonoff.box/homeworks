// Теоретичне питання
1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
Prototype inheritance in javascript is the linking of prototypes of a parent object to a child object to share and utilize the properties of a parent class using a child class.
2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
The keyword 'super' is used to access properties on an object literal or class's Prototype, or call a superclass' constructor.