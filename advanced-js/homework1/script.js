// Теоретичне питання
// 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Prototype inheritance in javascript is the linking of prototypes of a parent object to a child object to share and utilize the properties of a parent class using a child class.
// 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
// The keyword 'super' is used to access properties on an object literal or class's Prototype, or call a superclass' constructor.


class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get employeeName() {
        return this.name;
    }
    set employeeName(value) {
        this.name = value;
    }

    get employeeAge() {
        return this.age;
    }
    set employeeAge(value) {
        this.age = value;
    }

    get employeeSalary() {
        return this.salary;
    }
    set employeeSalary(value) {
        this.salary = value;
    }
}

class Programmer extends Employee {
    constructor(lang, ...args) {
        super(...args);
        this.lang = lang;
    }

    get employeeSalary() {
        return this.salary * 3;
    }
}

const programmer1 = new Programmer('Spanish', 'Ivan', 36, 2500);
const programmer2 = new Programmer('English', 'John', 41, 2100);
const programmer3 = new Programmer('French', 'Francois', 37, 2350);

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);