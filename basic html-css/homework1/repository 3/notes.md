1. First scenario: we need to add new homework to new repository on Gitlab.
- we create a new project on Gitlab.com by pressing button "New project". We copy the URL-path of this project.
- on the local PC, we choose respective spot (folder) where we are planning to store our new homework. We open Gitbash in this folder.
- we use "git clone" command, also indicating the URL copied in step 1.
- we add homework files to a local PC, then we use git commands: "git add .", "git commit -m "comment"", and "git push" in order to update the remote repository on Gitlab.

2. Second scenario: we need to upload existing homework on our local PC to a new repository on Gitlab.
- we initialize git in respective folder on a local PC by typing "git init" in gitbash terminal.
- we use "git remote add origin" command + we indicate the URL-path to the newly created repository on Gitlab.
- in order to upload the changes made to the homework, we use commands indicated in step 4 of the first scenario.